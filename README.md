# Data Structures and Algorithms

This notebook compiles my notes and solved exercises of the great Udemy course [Data Structures and Algorithms Bootcamp in Python](https://www.udemy.com/course/data-structures-and-algorithms-bootcamp-in-python/) taken between March and June 2023.

The course instructor is **Elshad Karimov**, who I want to thank his work making this course that helped me understand the theoretical aspects of the algorithmic world.